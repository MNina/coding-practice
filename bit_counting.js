/*Write a function that takes an integer as input, and returns the number of bits that are equal 
to one in the binary representation of that number. You can guarantee that input is non-negative.

Example: The binary representation of 1234 is 10011010010, so the function should return 5 in this case+ 
*/
/*countBits = n => n.toString(2).split('0').join('').length;*/

var countBits = function(n) {
    // intiger prebacujem u binarni //
      function dec2bin(dec){
        return (dec >>> 0).toString(2);
    }
    var brBin = dec2bin(n);
    //  sabiram sve cifre u rezultatu
    var sum =0;
    for (var member in brBin) {
            sum += parseInt(brBin[member]);
      }
      //   i zbir prikazujem kao rezultat 
      return sum
    };


console.log(countBits(1234))