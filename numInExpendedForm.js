// writing numbers in Expanded Form via javascript

function expandedForm(num) {
    const arrStr = num
     .toString()
     .split('');
  
    for (let i = 0; i < arrStr.length - 1; ++i) {
      if (arrStr[i] > 0) {
     for (let j = i; j < arrStr.length - 1; ++j) {
        arrStr[i] += '0';
     }
    }
   }
    return arrStr
     .join()
     .replace(new RegExp(",0", "g"), "")
     .replace(new RegExp(",", "g"), " + ");
  }

  console.log(expandedForm(309));