function getSum( a,b )
{   
    if(a===b){
        return a;
    }
    var lowEnd;
    var highEnd;

    if(a > b){
        highEnd = a;
        lowEnd = b;
    }
    else {
        highEnd = b;
        lowEnd = a;
    }
    var arr = [];
    for(let i = lowEnd; i <= highEnd; i++){
        arr.push(i);
    }
    return arr.reduce((a, b) => a+b , 0);
}
console.log(getSum(0,-1));
console.log(getSum(-1,-1));
console.log(getSum(-1,0));
console.log(getSum(2,5));


/*
 Proverim koje cifre imam izmedju zadatih brojeva
 navedem ih
 saberem sve brojeve izmedju mojih vrednosti
 i prikazem rezultat
 Ukoliko su brojevi isti prikazem samo 1 ne sabiram ih.
*/
const GetSum = (a, b) => {
    let min = Math.min(a, b),
        max = Math.max(a, b);
    return (max - min + 1) * (min + max) / 2;
  }